package com.example.dawid.recruitmenttaskskyrisedawidpyszora;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;

public class MainActivity extends AppCompatActivity {

    private TextView serchTeaxt;
    private Button searchButtton;
    private GoogleApiClient googleApiClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        serchTeaxt = (TextView) findViewById(R.id.search_text);
        searchButtton = (Button) findViewById(R.id.search_button);

        searchButtton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchMetod();
            }
        });

//        googleApiClient = new GoogleApiClient
//                .Builder(this)
//                .enableAutoManage(this, 0, this)
//                .addApi(Place.GEO_DATA_API)
//                .addApi(Place.PLACE_DETECTION)
//                .addConnectionCallbacks(this)
//                .addOnConnectionFailedListener(this)
//                .build();
    }
//    protected void searchPlace(View view) {
//
//    }
//        PlacePicker.IntentBuilder builder =  new PlacePicker.IntentBuilder();
//
//        try {
//            startActivityForResult(builder.build(MainActivity.this), 1);
//        } catch (GooglePlayServicesRepairableException e) {
//            e.printStackTrace();
//        } catch (GooglePlayServicesNotAvailableException e) {
//            e.printStackTrace();
//        }
//    }
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
////        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode ==1 ){
//            if (resultCode == RESULT_OK){
//                Place place = PlacePicker.getPlace(MainActivity.this, data);
//                serchTeaxt.setText(place.getAddress());
//            }
//        }
//    }

    //    @Override
//    protected void onStart() {
//        super.onStart();
//        if (googleApiClient != null) {
//            googleApiClient.connect();
//        }
//    }
//
//    @Override
//    protected void onStop() {
//        if (googleApiClient != null && googleApiClient.isConnected()) {
//            googleApiClient.disconnect();
//        }
//        super.onStop();
//    }

//    private void displayPlacePickier(Place place){
//        if (googleApiClient ==null || googleApiClient.isConnected())
//            return;
//
//        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
//
//        try {
//            startActivityForResult(builder.build((Activity) getApplicationContext()), 1);
//        } catch (GooglePlayServicesRepairableException e) {
//            e.printStackTrace();
//        } catch (GooglePlayServicesNotAvailableException e) {
//            e.printStackTrace();
//        }
//    }
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (requestCode ==1 &&resultCode == RESULT_OK){
//            displayPlacePickier(PlacePicker.getPlace(this, data));
//        }
//    }
    //    public void initViewComponent() {
//
//    }
//
    public void searchMetod() {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            Intent intent = builder.build(this);
            startActivityForResult(intent, 1);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(this, data);
                String toastMsg = String.format("Place: %s", place.getName());
                Toast.makeText(this, toastMsg, Toast.LENGTH_SHORT).show();
            }
        }
    }
}
